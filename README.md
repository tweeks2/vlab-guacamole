The VT "VLAB" Cloud Based V.D.I. System
=====================

![VLAB Block Diagram](misc/vlab-block-diagram.png)

This repo manages the Docker containers configs and automation scripts
of the Virginia Tech VLAB Guacamole instance.. a cloud based Virtual
Desktop for Instruction system that situates an Apache Guacamole
web-to-RDP gateway system in front of cloud desktop VM systems
for use by students and instructors.

This implementation uses docker containers for the guac and guacd
components, as well as docker proxy (nginx) container that handles
user facing TLS sessions, and directing the decrypted client sessions
internally from nginx to guacamole (on 8080), which then connects the
client session back into the VM instructional desktops (via RDP ior ssh) 
within your cloud instance.  For more info, see - https://vacr.io/tweeks-vdi

This implementation includes a local MySQL authentication config  
(see mysql.yml) as well as a more advanced campus CAS/SAML authentication 
token config that uses your campus auth tokens so that no student 
ID/FERPA information needs hit the cloud (see stack.yml)

To limit the number of things you need to troubleshoot in getting this
configuration up and running, I recommend initially setting up using a
local SQL for user authentication (see guacadmin login in the 
db/initialize-db.sql file) via the mysql.yml configuration.  Once you
have this working and you create new admin users (who's SQL auth matching 
your campus auth ID), then you can start playing  with centralized CAS 
authentication for your institution, and then possibly moving SQL to a 
cloud DB provider as seen in stack.yml (if desired).

If you are not already shutting down your end user VMs when not in use,
there are some things that can be done from either the docker output/logs
regarding tracking user sessions, or by tracking RDP sessions and 
automating start/stop actions via awscli (if using AWS).

This proof of concept implementation is also designed to be installed
and run out of /root/vlab-guacamole/ . Additionally, if running on a 
non-ephemeral system that is not regularly reprovisioned, then you will 
want to also consider implementing the cron jobs:

* vlab-cron-cert-update.sh - For semi-automating Lets Encrypt TLS cert updates
* vlab-cron-cleanup-prune.sh - for docker and apt-package cleanup AWS issues

Send us any feedback, config fixes or suggestions you have in improving
our "Virtual Lab" setup.

Implementation covered under 2019 Creative Commons v4
(CC) (AS) (BY)
https://creativecommons.org/licenses/by-sa/4.0/legalcode
