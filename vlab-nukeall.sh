#!/bin/bash

echo "!!! THIS WILL REMOVE NUKE ALL DOCKER IMAGES FROM SPACE !!!"
echo "             (it's the only way to be sure...)"
echo "ENTER to continue, CTRL-C to ABORT"
read


./vlab-stop.sh 
docker system prune 
docker image rm guacamole/guacamole guacamole/guacd guacamole-proxy nginx

echo
echo "All Done.. ready to create everything with ./vlab-build.sh and ./vlab-start.sh"
echo

