#!/bin/bash

## For things to work correctly on reboot, ensure the system is configured to 
## autostart the dockerd service.

cd /root/vlab-guacamole/
docker-compose -p guac -f stack.yml up -d
cd -

