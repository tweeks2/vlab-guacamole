#!/bin/bash
## vlab-cron-cleanup-prune.sh
## By T.Weeks@vt.edu
## Monthly cronjob clean up script for preventing docker overlay2 files from
## filling up the /var/lib/ partition and crashing the system.. as well as
## doing an apt-get autoremove, which (for frequently upgraded kernels, such
## as with AWS), will clean up/remove Gigs of old linux-header kernel source
## files.

if [[ $USER = "root" ]]; then
	echo "Cleaning up apt & docker cruft..."
	apt-get autoremove -y && docker system prune -f	
	echo "Done"
else
	echo "ERROR: must be run as root user"
fi



