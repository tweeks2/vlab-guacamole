#!/bin/bash

## For things to work correctly on reboot, ensure the system is configured to 
## autostart the dockerd service.

cd /root/vlab-guacamole/
docker stop  guac_guacamole_1  guac_guacd_1 guac_proxy_1
#docker-compose -p guac -f stack.yml down -d
cd -

