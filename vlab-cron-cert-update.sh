#!/bin/bash
## vlab-cron-cert-update.sh
## By: T.weeks@vt.edu
## Most of this script is from LetsEncrypt for automating the 
## LE TLS certs. However, this TLS/Lets Encrypt script is not 
## fully automated yet for setting this up and symlinking 
## for nginx running within a docker container.
## If run, you must manually follow up by resetting up the TLS
## symlinks and rebuilding the nginex container.

# Testing switch
if [[ $1 == "-t" ]]; then
	export TEST="echo TEST-ECHO: "
else
	export TEST=""
fi

cd /root/vlab-guacamole

# Stop all docker containers & Start up nginx
echo "Stopping docker containers..."
$TEST docker stop  guac_guacamole_1  guac_guacd_1 guac_proxy_1
sleep 5
echo "Starting nginx..."
$TEST service nginx start
export STATUS=$(netstat -antp|grep :80|grep nginx >/dev/null 2>&1;echo $?)
if [[ $STATUS == "0" ]];then
	echo "nginx running... continue.."
	export STATUS=""
else
	echo "nginx not running.. ABORT."
	export STATUS=""
	exit
fi

# Update vlab cert(s)
$TEST certbot -n --nginx certonly -d vlab.cloud.vt.edu
export STATUS=$?



if [[ $STATUS == 0 ]]; then
	cd /root/vlab-guacamole
	# Shut down Guac containers
	$TEST docker-compose -p guac -f stack.yml down
	$TEST killall nginx	# just to make sure nothing's on port 80
	# Relink New Cert & key files
	$TEST export NEWCERT=$(ls -rt /etc/letsencrypt/live/vlab.cloud.vt.edu/fullchain*.pem | tail -1)
	$TEST export NEWKEY=$(ls -rt /etc/letsencrypt/live/vlab.cloud.vt.edu/privkey*.pem | tail -1)
	$TEST ln -fs $NEWCERT cert.pem
	$TEST ln -fs $NEWKEY key.pem
	# Rebuild containers
	$TEST ./build.sh 
	# Start up containers
	$TEST docker-compose -p guac -f stack.yml up -d 
	# Test 443 port binding
	$TEST netstat -antp|grep :443
	if [[ $? == "0" ]];then
		echo "OK: Looks good..."
		docker ps -a
	else
		echo "ERROR: No 443 port binding detected..."
	fi
else
	$TEST echo "STATUS: certbot failed... not rebuilding container.."
	$TEST echo "do something?"
	docker-compose -p guac -f stack.yml up -d
fi

echo #########################################################################
echo #### NEED TO UPDATE CERT SYMLINKS AND REBUILD NGINX CONTAINER ###########
echo #########################################################################


cd -

